# commentariat-tilix
A port of the commentariat colour scheme to the Tilix terminal emulator. Pastels, medium contrast, and distinctive colours.

# To install

Place `commentariat.json` into your `tilix/schemes/` folder. On Linux flavours, this is usually `~/.config/Tilix/schemes/`. 

# Screenshot

![How it looks with powerline10k installed on zsh](./screenshots/terminal.png)

# Commentariat on other programs

See the [commentariat atom theme](https://github.com/anthbrtn/commentariat) for a markdown-based writing syntax theme for atom.
